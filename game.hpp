#pragma once

#include <cstdint>
#include <memory>

namespace GameOfLife
{
    using DWord = uint32_t;
    using SharedCellPtr = std::shared_ptr<DWord[]>;
    using NeighbouringCoordinates = std::tuple<int, int>[8];

    constexpr DWord ALIVE{0xffffffff};
    constexpr DWord DEAD{0x00000000};

    /*
    Moore neighbourhood.

    The Moore neighbourhood is composed of nine cells: a central cell(x) and the
    eight cells which surround it(o).

    +----+----+----+----+
    | oo | oo | oo |    |
    | oo | oo | oo |    |
    +----+----+----+----+
    | oo | xx | oo |    |
    | oo | xx | oo |    |
    +----+----+----+----+
    | oo | oo | oo |    |
    | oo | oo | oo |    |
    +----+----+----+----+

    There are 8 coordinates (x, y) of interest with the central cell as the origin:
    Northwest: (-1, -1)
    North:     ( 0, -1)
    Northeast: ( 1, -1)
    West:      (-1,  0)
    East:      ( 1,  0)
    Southwest: (-1,  1)
    South:     ( 0,  1)
    Southeast: ( 1,  1)

    */
    
    constexpr NeighbouringCoordinates NEIGHBOURING_COORDS{
        {-1, -1},
        {0, -1},
        {1, -1},
        {-1, 0},
        {1, 0},
        {-1, 1},
        {0, 1},
        {1, 1},
    };

    class Game
    {
    public:
        Game() = delete;
        Game(size_t width, size_t height);
        ~Game() = default;
        Game(const Game &other) = delete;
        Game(Game &&other) noexcept = delete;
        Game &operator=(const Game &other) = delete;
        Game &operator=(Game &&other) noexcept = delete;

        void advance_generation();
        const SharedCellPtr get_board() const;

    private:
        const size_t get_neighbours(size_t x, size_t y) const;
        DWord &get_cell(size_t, size_t y);
        DWord &get_cell(SharedCellPtr board, size_t, size_t y) const;
        const DWord &get_cell(size_t x, size_t y) const;

        size_t width;
        size_t height;
        SharedCellPtr board;
    };
}