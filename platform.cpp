#include "platform.hpp"

#include <stdexcept>

namespace GameOfLife
{
    Platform::Platform(const char *title,
                       size_t window_width,
                       size_t window_height,
                       size_t texture_width,
                       size_t texture_height)
        : window{},
          renderer{},
          texture{}
    {
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
        {
            throw std::runtime_error{"Failed to initialize SDL: " + std::string{SDL_GetError()}};
        }

        if (window = SDL_CreateWindow(title,
                                      0, 0,
                                      window_width, window_height,
                                      SDL_WINDOW_SHOWN);
            !window)
        {
            throw std::runtime_error{"Failed to initialize window: " + std::string{SDL_GetError()}};
        }

        if (renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED); !renderer)
        {
            throw std::runtime_error{"Failed to initialize renderer: " + std::string{SDL_GetError()}};
        }

        if (texture = SDL_CreateTexture(renderer,
                                        SDL_PIXELFORMAT_RGBA8888,
                                        SDL_TEXTUREACCESS_STREAMING,
                                        texture_width, texture_height);
            !texture)
        {
            throw std::runtime_error{"Failed to initialize texture: " + std::string{SDL_GetError()}};
        }
    }

    Platform::~Platform()
    {
        SDL_DestroyTexture(texture);
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
    }

    bool Platform::process_input() const
    {
        bool quit{false};
        SDL_Event event{};

        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
            {
                quit = true;
            }
            break;

            case SDL_KEYDOWN:
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                {
                    quit = true;
                }
                break;

                default:
                {
                }
                break;
                }
            }
            break;

            default:
            {
            }
            break;
            }
        }

        return quit;
    }

    void Platform::update(const SharedCellPtr board, int pitch)
    {
        if (SDL_UpdateTexture(texture, nullptr, board.get(), pitch) < 0)
        {
            throw std::runtime_error{"Failed to update texture: " + std::string{SDL_GetError()}};
        }

        if (SDL_RenderClear(renderer) < 0)
        {

            throw std::runtime_error{"Failed to clear renderer: " + std::string{SDL_GetError()}};
        }

        if (SDL_RenderCopy(renderer, texture, nullptr, nullptr) < 0)
        {
            throw std::runtime_error{"Failed to copy renderer: " + std::string{SDL_GetError()}};
        }

        SDL_RenderPresent(renderer);
    }
}