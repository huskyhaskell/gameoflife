#include "game.hpp"
#include "platform.hpp"

#include <stdexcept>

using namespace GameOfLife;

int main(int argc, char **argv)
{
    try
    {
        if (argc < 3)
        {
            throw std::runtime_error{"Usage: ./gameoflife [WIDTH] [HEIGHT]"};
        }

        size_t width{std::stoul(argv[1])};
        size_t height{std::stoul(argv[2])};
        auto video_pitch{sizeof(DWord) * width};

        Game game{width, height};
        Platform platform{"GAME OF LIFE",
                          width, height,
                          width, height};

        auto quit{false};

        while (!quit)
        {
            quit = platform.process_input();
            platform.update(game.get_board(), video_pitch);
            game.advance_generation();
        }
    }
    catch (const std::exception &e)
    {
        printf("%s\n", e.what());
        return 1;
    }

    return 0;
}