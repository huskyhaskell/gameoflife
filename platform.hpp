#pragma once

#include "game.hpp"

#include <SDL2/SDL.h>

namespace GameOfLife
{
    class Platform
    {
    public:
        Platform() = delete;
        Platform(const char *title,
                 size_t window_width,
                 size_t window_height,
                 size_t texture_width,
                 size_t texture_height);
        ~Platform();
        Platform(const Platform &other) = delete;
        Platform(Platform &&other) noexcept = delete;
        Platform& operator=(const Platform &other) = delete;
        Platform& operator=(Platform &&other) noexcept = delete;

        bool process_input() const;
        void update(const SharedCellPtr board, int pitch);

    private:
        SDL_Window *window;
        SDL_Renderer *renderer;
        SDL_Texture *texture;
    };
}