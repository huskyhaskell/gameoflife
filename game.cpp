#include "game.hpp"

#include <stdexcept>
#include <ctime>

namespace GameOfLife
{
    Game::Game(size_t width, size_t height)
        : width{width},
          height{height},
          board{new DWord[width * height]{DEAD}}
    {
        //std::srand(std::time(nullptr));

        //for (size_t row{0}; row < height; row++)
        //{
        //    for (size_t col{0}; col < width; col++)
        //    {
        //        if (std::rand() % 2)
        //        {
        //            get_cell(col, row) = ALIVE;
        //        }
        //    }
        //}

        for (size_t i{1}; i < height - 1; i++)
        {
            board[1 + i * width] = ALIVE;
            board[(width - 2) + i * width] = ALIVE;
        }

        for (size_t i{2}; i < width - 2; i++)
        {
            board[width + i] = ALIVE;
            board[(height - 2) * width + i] = ALIVE;
        }
    }

    const SharedCellPtr Game::get_board() const
    {
        return board;
    }

    void Game::advance_generation()
    {
        SharedCellPtr new_board{new DWord[width * height]};

        for (size_t row{0}; row < height; row++)
        {
            for (size_t col{0}; col < width; col++)
            {
                const auto neighbours_alive{get_neighbours(col, row)};
                auto &new_cell{get_cell(new_board, col, row)};

                if (const auto &old_cell{get_cell(col, row)};
                    old_cell == ALIVE &&
                    (neighbours_alive < 2 || neighbours_alive > 3))
                {
                    new_cell = DEAD;
                }
                else if (neighbours_alive == 3)
                {
                    new_cell = ALIVE;
                }
                else
                {
                    new_cell = old_cell;
                }
            }
        }

        board = std::move(new_board);
    }

    const DWord &Game::get_cell(size_t x, size_t y) const
    {
        if (const auto idx{y * width + x}; idx < (width * height))
        {
            return board[idx];
        }
        else
        {
            throw std::out_of_range{"ERROR: Tried to access cell at x: " + std::to_string(x) + ", y: " + std::to_string(y)};
        }
    }

    DWord &Game::get_cell(size_t x, size_t y)
    {
        if (const auto idx{y * width + x}; idx < (width * height))
        {
            return board[idx];
        }
        else
        {
            throw std::out_of_range{"ERROR: Tried to access cell at x: " + std::to_string(x) + ", y: " + std::to_string(y)};
        }
    }

    DWord &Game::get_cell(SharedCellPtr other_board, size_t x, size_t y) const
    {
        if (const auto idx{y * width + x}; idx < (width * height))
        {
            return other_board[idx];
        }
        else
        {
            throw std::out_of_range{"ERROR: Tried to access cell at x: " + std::to_string(x) + ", y: " + std::to_string(y)};
        }
    }

    const size_t Game::get_neighbours(size_t x, size_t y) const
    {
        auto count{0};

        for (const auto &[x_offset, y_offset] : NEIGHBOURING_COORDS)
        {
            const auto peek_x{x + x_offset};
            const auto peek_y{y + y_offset};

            if (0 <= peek_x && peek_x < width &&
                0 <= peek_y && peek_y < height &&
                board[peek_y * width + peek_x] == ALIVE)
            {
                count++;
            }
        }

        return count;
    }
}