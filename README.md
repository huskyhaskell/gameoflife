# Game of Life

![Game of Life](./assets/gameoflife.png)

## Building
```shell
$ make release # for optimized build
$ make debug # for debug build
```