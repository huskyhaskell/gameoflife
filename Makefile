CC = g++ -Wall -Werror -std=c++17
CCDEBUG = $(CC) -g -fsanitize=address -static-libasan
CCRELEASE = $(CC) -O2

release: game.o platform.o main.cpp
	$(CCRELEASE) game.o platform.o main.cpp -o gameoflife -lSDL2

debug: game.o platform.o main.cpp
	$(CCDEBUG) game.o platform.o main.cpp -o gameoflife -lSDL2

platform.o: platform.hpp platform.cpp
	$(CC) -c platform.cpp

game.o: game.hpp game.cpp
	$(CC) -c game.cpp
	
clean:
	rm platform.o game.o gameoflife